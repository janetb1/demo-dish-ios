//
//  LoginProgressViewController.swift
//  Dish Demo
//
//  Created by Janet Brumbaugh on 5/2/18.
//  Copyright © 2018 Averon. All rights reserved.
//

import UIKit

class LoginProgressViewController: UIViewController {

    @IBOutlet weak var averonLogo: UIImageView!
    @IBOutlet weak var lblLoggingIn: UILabel!
    @IBOutlet weak var averonLogoView: UIView!
    @IBOutlet weak var loginCompleteView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Don't set visibility to hidden.  Instead, set the alpha to 0
        // That way we can animate it with alpha = 1 to make it 'visible' smoothly
        self.loginCompleteView.alpha = 0.0
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
   
        // Note that the newer UIPropertyAnimator does not offer a repeat count.
        UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseInOut,
                .autoreverse,
                .repeat
            ],
            animations: {
                // Use 2.5 cycles, so these views end up with alpha 0.0.
                UIView.setAnimationRepeatCount(2.5)
                self.lblLoggingIn.alpha = 0.0
                self.averonLogo.alpha = 0.0
            },
            completion: {
                (finished: Bool) -> Void in
                
                /* Animate the fade in for the login complete view.
                 This animation is done because simply setting hiddento false looks abrupt.
                */
                UIView.animate(withDuration: 1.0, delay: 0.0, options: [UIViewAnimationOptions.curveEaseInOut],
                    animations: {
                        self.averonLogo.alpha = 1.0
                        self.loginCompleteView.alpha = 1.0
                    },
                    completion: {
                        /* Let the user view the login complete text
                            for a few seconds, then nav to the dashboard */
                        (finished: Bool) -> Void in DispatchQueue.main.asyncAfter(deadline: .now() + 1.5 ) {
                            self.performSegue(withIdentifier: "LoginProgressToDashboardSegue", sender: self);
                         }
                     }
                )
            }
        )
    }
    
   


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
